set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" add all your plugins here (note older versions of Vundle
" used Bundle instead of Plugin)

Plugin 'tmhedberg/SimpylFold'
Plugin 'scrooloose/nerdtree'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'sheerun/vim-polyglot'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'Valloric/YouCompleteMe'

" theme
"Plugin 'joshdick/onedark.vim'
Plugin 'dylanaraps/wal.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" map leader to ','
let mapleader = ","
let g:mapleader = ","

" set 7 lines to the cursor
set so=7

" turn on the wildmenu
set wildmenu

" ignore compiled files
set wildignore=*.o,*~,*.pyc

" always show current position
set ruler

" set column width to be 80 chars
set colorcolumn=80

" configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" ignore case when searching
set ignorecase

" when searching try to be smart about cases
set smartcase

" highlight search results
set hlsearch

" makes search act like search in modern browsers
set incsearch

" don't redraw while executing macros
set lazyredraw

" show matching brackets when text indicator is over them
set showmatch

" how many tenths of a second to blink when matching brackets
set mat=2

"extra martgin to the left
set foldcolumn=1

" turn backup off
set nobackup
set nowb
set noswapfile

" spaces instead of tabs
set expandtab

" be smart when using tabs
set smarttab

" Splits
set splitbelow
set splitright

" split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>


" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar
nnoremap <space> za

" Docstrings for folded code
let g:SimpylFold_docstring_preview=1

" Flag unnecessary whitespace
highlight BadWhitespace ctermbg=red guibg=darkred
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" UTF-8
set encoding=utf-8

" pretty code
let python_highlight_all=1
syntax on

" ignore .pyc in nerdtree
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

" toggle nerdTree
map <C-n> :NERDTreeToggle<CR>

" close vim if the only window open is nerdTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" line numbering
set nu
set number relativenumber

" theme
set background=dark
:color elflord
" colorscheme onedark
colorscheme wal

" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='angr'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=0 expandtab
